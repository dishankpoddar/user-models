
from django_otp.models import Device
from django_otp.oath import TOTP
from django_otp.util import random_hex, hex_validator
from binascii import unhexlify
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.conf import settings

import time

from .models import User

def default_key():
    return random_hex(20)

class VerificationDevice(Device):
    unverified_phone = models.CharField(max_length=16)
    secret_key = models.CharField(
        max_length=40,
        default=default_key,
        validators=[hex_validator],
        help_text=_("Hex-encoded secret key to generate totp OTPs."),
        unique=True,
    )
    last_verified_counter = models.BigIntegerField(
        default=-1,
        help_text=_("The counter value of the latest verified OTP."
                   "The next OTP must be at a higher counter value."
                   "It makes sure a OTP is used only once.")
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    label = models.CharField(max_length=20,
                            choices=(('account_verify', _("Verify Account")),
                                    ('phone_verify', _("Verify Phone")),
                                    ('password_reset', _("Reset Password"))),
                            default='account_verify')
    verified = models.BooleanField(default=False)

    step = settings.TOTP_OTP_VALIDITY
    digits = settings.TOTP_DIGITS

    class Meta(Device.Meta):
        verbose_name = "Verification Device"

    @property
    def bin_key(self):
        return unhexlify(self.secret_key)

    def totp_obj(self):
        totp = TOTP(key=self.bin_key, step=self.step, digits=self.digits)
        totp.time = time.time()
        return totp

    def generate_challenge(self):
        totp = self.totp_obj()
        otp = str(totp.token()).zfill(self.digits)

        message = _(f'Your OTP for Impava is {otp}.\n'
                    f'It is valid for {self.step // 60} minutes.')

        print(self.unverified_phone, message)

        return otp

    def verify_otp(self, otp, tolerance=0):
        totp = self.totp_obj()
        if ((totp.t() > self.last_verified_counter) and
                (totp.verify(otp, tolerance=tolerance))):
            self.last_verified_counter = totp.t()
            self.verified = True
            self.save()
        else:
            self.verified = False
        return self.verified
