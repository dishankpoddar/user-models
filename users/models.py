from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.db.models import Q
import uuid
from phonenumber_field.modelfields import PhoneNumberField

class UserManager(BaseUserManager):
    """
    Custom user model manager where email is the unique identifiers
    for authentication instead of usernames.
    """
    def normalize_phone(self, phone):
        if phone:
            phone = phone.strip().lower()
            try:
                import phonenumbers
                phone_number = phonenumbers.parse(phone)
                phone = phonenumbers.format_number(
                    phone_number, phonenumbers.PhoneNumberFormat.E164)
            except ImportError:
                pass

        return phone

    def create_user(self, username, password, email=None, phone=None, role=None):
        """
        Create and save a User with the given email and password.
        """
        if not role:
            role = User.UNDEFINED
        email = self.normalize_email(email)
        phone = self.normalize_phone(phone)
        user = self.model(username=username,
            email=email, 
            phone = phone,
            role = role)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, email=None, phone=None, role=None):
        """
        Create and save a SuperUser with the given email and password.
        """
        role = User.SUPER_USER
        email = self.normalize_email(email)
        phone = self.normalize_phone(phone)
        user = self.model(username=username,
            email=email, 
            phone = phone,
            role = role)
        user.set_password(password)
        user.is_staff = True
        user.is_superuser = True
        user.save()
        return user


class User(AbstractUser):
    """
        Custom User has fields First Name, Last Name, Email Address,
        Phone Number, and Role in the website
    """
    class Meta:
        constraints = [
            models.CheckConstraint(
                check = Q('email'!=None)|Q('phone'!=None),
                name  = 'email_or_phone_filled',
            ),
        ]

    id = models.UUIDField( 
         primary_key = True, 
         default = uuid.uuid4, 
         editable = False)
    email = models.EmailField(_('Email Address'), unique=True,null=True,blank=True)
    phone = PhoneNumberField(_('Phone Number'), unique=True,null=True,blank=True)

    SUPER_USER = 'SU'
    IMPAVA_ADMIN = 'IA'
    GROUP_ADMIN = 'TE'
    FUNDRAISER = 'FR'
    DONOR = 'DO'
    UNDEFINED = 'UD'
    ROLE_CHOICES = [
        (IMPAVA_ADMIN,'Impava Admin'),
        (GROUP_ADMIN,'Group Admin'),
        (FUNDRAISER, 'Fundraiser'),
        (DONOR, 'Donor'),
    ]
    role = models.CharField(max_length=2,choices=ROLE_CHOICES,default=UNDEFINED,)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []


    def __str__(self):
        return f'{self.username}'


class ImpavaAdmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    def __str__(self):
        return f'{self.user.username}'

class GroupAdmin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    #group = models.ForeignKey(Group, on_delete=?,related_name='group_admin')
    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

class Fundraiser(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'

class Donor(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}'
