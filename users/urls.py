from django.urls import path
from . import views
from django.contrib.auth import views as auth_views
from .forms import CustomAuthenticationForm 

urlpatterns = [
    path('', views.generic_dashboard, name='index'),
    path('dashboard/',views.generic_dashboard, name='index'),
    path('dashboard/admin/',views.impava_admin_dashboard, name='impava-admin-dashboard'),
    path('group/admin/',views.group_admin_dashboard, name='group-admin-dashboard'),
    path('dashboard/fundraiser',views.fundraiser_dashboard, name='fundraiser-dashboard'),
    path('dashboard/donor',views.donor_dashboard, name='donor-dashboard'),
    path('signup/fundraiser',views.fundraiser_signup, name='signup'),
    path('signup/donor',views.donor_signup, name='donor-signup'),
    path('activate/<uidb64>/<token>',views.activate_via_email, name='activate-email'),
    path('phone/<uuid:id>',views.VerifyPhone.as_view(), name='activate-phone'),
    path('login/', auth_views.LoginView.as_view(
                        template_name='users/login.html',
                        form_class=CustomAuthenticationForm),name="login"),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'),name="logout"),
    path('password-reset/', auth_views.PasswordResetView.as_view(template_name='users/password_reset.html'),name="password_reset"),
    path('password-reset-confirm/<uidb64>/<token>', auth_views.PasswordResetConfirmView.as_view(template_name='users/password_reset_confirm.html'),name="password_reset_confirm"),
    path('password-reset-done', auth_views.PasswordResetDoneView.as_view(template_name='users/password_reset_done.html'),name="password_reset_done"),
    path('password-reset-complete', auth_views.PasswordResetCompleteView.as_view(template_name='users/password_reset_complete.html'),name="password_reset_complete"),
]
