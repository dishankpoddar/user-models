from django import forms
from django.db import transaction
from django.forms.utils import ValidationError
from django.core.validators import validate_email,RegexValidator
import phonenumbers
from .models import User,Fundraiser,Donor
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import (
    UserCreationForm, AuthenticationForm, PasswordResetForm, SetPasswordForm,
)
from django.contrib.auth.password_validation import validate_password
from django.contrib.auth import authenticate
import random
import string
from django.conf import settings

USERNAME_SUFFIX_LENGTH = 8

def random_username_suffix():
    return ''.join(random.choice(string.ascii_letters) for _ in range(USERNAME_SUFFIX_LENGTH))

def valid_as_email(email):
    try:
        validate_email(email)
    except:
        return False
    return True

def valid_as_phone(phone):
    formatted_phone = phonenumbers.parse(phone)
    return phonenumbers.is_valid_number(formatted_phone)

class EmailOrPhoneField(forms.CharField):
    def clean(self, value):
        try:
            assert(valid_as_email(value) or valid_as_phone(value))
            return value
        except:
            raise ValidationError(_("Please enter a valid email address or phone number"))
        
class SignUpForm(UserCreationForm):
    email_or_phone = EmailOrPhoneField(
        label = _("Email Address or Phone Number")
    )
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('first_name','last_name','email_or_phone','password1','password2')
    
    def clean_email_or_phone(self):
        cleaned_data = super().clean()
        email_or_phone = cleaned_data.get("email_or_phone")
        if("@" in email_or_phone):
            email_or_phone = email_or_phone.casefold()
            if(User.objects.filter(email=email_or_phone).first()):
                raise ValidationError(_(f"User with this email address ({email_or_phone}) already exists"))
        else:
            if(User.objects.filter(phone=email_or_phone).first()):
                raise ValidationError(_(f"User with this phone number ({email_or_phone}) already exists"))
        
        return email_or_phone

    def clean_password1(self):
        password = self.data.get('password1')
        validate_password(password)
        errors = []

        email_or_phone = self.data.get('email_or_phone')
        if "@" in email_or_phone:
            email = email_or_phone.split('@')
            if email[0].casefold() in password.casefold():
                errors.append(_("Passwords cannot contain your email."))
        else:
            try:
                phone = str(phonenumbers.parse(email_or_phone).national_number)
                if phone in password:
                    errors.append(_("Passwords cannot contain your phone."))
            except phonenumbers.NumberParseException:
                pass

        if errors:
            raise forms.ValidationError(errors)

        return password

    @transaction.atomic
    def save(self, commit=True):
        user = super().save(commit=False)
        while(True):
            username = f'{user.first_name}_{user.last_name}_{random_username_suffix()}'
            existing_user = User.objects.filter(username=username).first()
            if(not existing_user):
                break
        email_or_phone = self.data['email_or_phone']
        if("@" in email_or_phone):
            user.email = email_or_phone
            user.phone = None
        else:
            user.email = None
            user.phone = email_or_phone
        user.username = username
        user.role = self.role
        user.is_active = False
        user.save()
        return user

ERROR_MESSAGE = _("Please enter a correct email and password. ")
ERROR_MESSAGE_RESTRICTED = _("You do not have permission to access the admin.")
ERROR_MESSAGE_INACTIVE = _("This account is inactive.")

class CustomAuthenticationForm(AuthenticationForm):
    email_or_phone = EmailOrPhoneField(
        label = _("Email Address or Phone Number")
    )
    message_incorrect_password = ERROR_MESSAGE
    message_inactive = ERROR_MESSAGE_INACTIVE

    class Meta:
        model = User
        fields = ["email_or_phone", "password"]
        #field_order = ['email_or_phone', 'password']

    def __init__(self, request=None, *args, **kwargs):
        super(CustomAuthenticationForm, self).__init__(request, *args, **kwargs)
        del self.fields['username']
        self.order_fields(['email_or_phone', 'password'])
        for visible in self.visible_fields():
            if visible.name == 'email_or_phone':
                visible.field.widget.attrs['placeholder'] = 'Email or Phone'
            if visible.name == 'password':
                visible.field.widget.attrs['placeholder'] = 'Password'
                visible.field.widget.attrs['autocomplete'] = 'off'
        return

    def clean(self):
        email_or_phone = self.cleaned_data.get('email_or_phone')
        password = self.cleaned_data.get('password')
        if email_or_phone and password:
            self.user_cache = authenticate(email_or_phone=email_or_phone, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(self.message_incorrect_password)
            if not self.user_cache.is_active:
                raise forms.ValidationError(self.message_inactive)
        return self.cleaned_data

class OTPVerificationForm(forms.Form):
    otp = forms.CharField(label=_("OTP"), max_length=settings.TOTP_DIGITS)

    def __init__(self, *args, **kwargs):
        self.device = kwargs.pop('device', None)
        super().__init__(*args, **kwargs)

    def clean_otp(self):
        otp = self.data.get('otp')
        device = self.device
        if not device:
            raise forms.ValidationError(
                _("The OTP could not be verified."
                    " Please click on 'here' to try again."))
        try:
            otp = int(otp)
        except ValueError:
            raise forms.ValidationError(_("OTP must be a number."))

        if device.verify_otp(otp):
            return otp
        elif device.verify_otp(otp=otp, tolerance=5):
            raise forms.ValidationError(
                _("The OTP has expired."
                  " Please click on 'here' to receive the new OTP."))
        else:
            raise forms.ValidationError(
               _("Invalid OTP. Enter a valid OTP."))
