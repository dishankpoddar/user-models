from .models import User

class EmailOrPhoneAuthenticationBackend:
    """
        Custom Authentication Backend that authenticates based on either email address or phone number of user
    """
    def authenticate(self, request, email_or_phone=None, password=None):
        try:
            if("@" in email_or_phone):
                email = email_or_phone
                user = User.objects.get(email = email)
            else:
                phone = email_or_phone
                user = User.objects.get(phone = phone)
            pwd_valid = user.check_password(password)
            if pwd_valid:            
                return user
            return None
        except User.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None