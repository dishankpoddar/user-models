from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import (
    User,
    Fundraiser,
    Donor,
    GroupAdmin,
    ImpavaAdmin,
)
@receiver(post_save, sender=User)
def create_user_connected_model(sender, **kwargs):
    if(kwargs['created']):
        if(kwargs['instance'].role==User.FUNDRAISER):
            fundraiser = Fundraiser(user=kwargs['instance'])
            fundraiser.save()
        if(kwargs['instance'].role==User.DONOR):
            donor = Donor(user=kwargs['instance'])
            donor.save()
        if(kwargs['instance'].role==User.IMPAVA_ADMIN):
            impava_admin = ImpavaAdmin(user=kwargs['instance'])
            impava_admin.save()
        if(kwargs['instance'].role==User.GROUP_ADMIN):
            group_admin = GroupAdmin(user=kwargs['instance'])
            group_admin.save()
