from django.shortcuts import render,HttpResponse, redirect,get_object_or_404
from django.conf import settings 
from django.contrib import messages
from .forms import SignUpForm,OTPVerificationForm
from .models import User,ImpavaAdmin,GroupAdmin,Fundraiser,Donor
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import (View,TemplateView,CreateView, DeleteView, DetailView, 
                                    ListView,UpdateView,FormView)
from django.db import transaction
from django.db.models import Avg, Count, F, Aggregate
from django.urls import reverse, reverse_lazy
from django.contrib.auth import views as auth_views

from .tokens import account_activation_token
from django.core.mail import EmailMessage
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth import login, authenticate
from django.template.loader import render_to_string

from django.utils.translation import ugettext_lazy as _
from .devices import VerificationDevice
from allauth.account.views import ConfirmEmailView, LoginView
from allauth.account.utils import send_email_confirmation
from allauth.account.models import EmailAddress
from allauth.account import signals

import json
from uuid import UUID
from binascii import unhexlify

class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            # if the obj is uuid, we simply return the value of uuid
            return obj.hex
        return json.JSONEncoder.default(self, obj)



def signup(request,role):
    if(request.method == 'POST'):
        form = SignUpForm(request.POST)
        if(form.is_valid()):
            form.role = role
            user = form.save()
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            if(user.email):
                # mail_subject = 'Activate your blog account.'
                # to_email = user.email
                # current_site = get_current_site(request)
                # message = render_to_string('users/activate_account_email.html', {
                #     'user': user,
                #     'domain': current_site.domain,
                #     'uid':urlsafe_base64_encode(force_bytes(user.pk)),
                #     'token':account_activation_token.make_token(user),
                # })
                # email = EmailMessage(
                #             mail_subject, message, to=[to_email]
                # )
                # email.send()
                send_email_confirmation(request, user)
            else:
                device = VerificationDevice.objects.create(
                    user=user, unverified_phone=user.phone)
                device.generate_challenge()
                print("This should work")
                #request.session['phone_verify_id'] = json.dumps(user.id, cls=UUIDEncoder)
            verify_method = "email address" if user.email else "phone number"
            messages.success(request,_(f'Account created for {first_name} {last_name}! Please verify your {verify_method} to login'))
            return redirect('activate-phone',user.id)
    else:
        form = SignUpForm()
    return render(request,'users/signup.html',{'form':form})

class VerifyPhone(FormView):
    template_name = 'users/account_verification_phone.html'
    form_class = OTPVerificationForm
    success_url = reverse_lazy('login')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        user_id = self.kwargs['id']
        emails_to_verify = EmailAddress.objects.filter(
            user_id=user_id, verified=False).exists()
        phones_to_verify = VerificationDevice.objects.filter(
            user_id=user_id, label='account_verify').exists()
        context['phone'] = phones_to_verify
        context['email'] = emails_to_verify

        return context

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        user_id = self.kwargs['id']
        try:
            form_kwargs['device'] = VerificationDevice.objects.get(
                user_id=user_id, label='account_verify')
        except VerificationDevice.DoesNotExist:
            pass
        return form_kwargs

    def form_valid(self, form):
        device = form.device
        user = device.user
        if user.phone != device.unverified_phone:
            user.phone = device.unverified_phone
        #user.phone_verified = True
        user.is_active = True
        user.save()
        device.delete()
        message = _("Successfully verified {phone}")
        message = message.format(phone=user.phone)
        messages.add_message(self.request, messages.SUCCESS, message)
        self.request.session.pop('phone_verify_id', None)
        return super().form_valid(form)



def activate_via_email(request, uidb64, token,backend='users.backends.EmailOrPhoneAuthenticationBackend'):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request, user,backend)
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation. Now you can login your account.')
    else:
        return HttpResponse('Activation link is invalid!')

def fundraiser_signup(request):
    return signup(request,User.FUNDRAISER)

def donor_signup(request):
    return signup(request,User.DONOR)




@login_required
def generic_dashboard(request):
    if request.user.is_authenticated:
        if request.user.role == User.IMPAVA_ADMIN:
            return redirect('impava-admin-dashboard')
        elif request.user.role == User.GROUP_ADMIN:
            return redirect('group-admin-dashboard')
        elif request.user.role == User.FUNDRAISER:
            return redirect('fundraiser-dashboard')
        elif request.user.role == User.DONOR:
            return redirect('donor-dashboard')
        else:
            return redirect('logout')

@login_required
def impava_admin_dashboard(request):
    return HttpResponse('Impava Admins Dashboard View')

@login_required
def group_admin_dashboard(request):
    return HttpResponse('Group Admins Dashboard View')

@login_required
def fundraiser_dashboard(request):
    return HttpResponse('Fundraiser Dashboard View')

@login_required
def donor_dashboard(request):
    return HttpResponse('Donor Dashboard View')