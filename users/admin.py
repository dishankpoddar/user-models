from django.contrib import admin
from .models import User,ImpavaAdmin,Fundraiser,Donor

# Register your models here.
admin.site.register(User)
admin.site.register(ImpavaAdmin)
admin.site.register(Fundraiser)
admin.site.register(Donor)