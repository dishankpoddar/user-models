from django import forms
from ald_api.models import (
    User, ContractMaster, MandateMaster, NACHBillingInput,
    NACHProcessedData, NACHTXNReport, DebitDayDefault, DebitDayOverride,
    TDSApplicable, MandateDebitOverride,
)
from django.contrib.auth.forms import (
    UserCreationForm, AuthenticationForm, PasswordResetForm, SetPasswordForm,
)
from django.contrib.auth import authenticate
from django.utils.translation import ugettext as _

ERROR_MESSAGE = _("Please enter a correct email and password. ")
ERROR_MESSAGE_RESTRICTED = _("You do not have permission to access the admin.")
ERROR_MESSAGE_INACTIVE = _("This account is inactive.")


class CustomAuthenticationForm(AuthenticationForm):
    email = forms.EmailField(required=True)
    message_incorrect_password = ERROR_MESSAGE
    message_inactive = ERROR_MESSAGE_INACTIVE

    class Meta:
        model = User
        fields = ("email", "password")

    def __init__(self, request=None, *args, **kwargs):
        super(CustomAuthenticationForm, self).__init__(request, *args, **kwargs)
        del self.fields['username']
        self.fields.keyOrder = ['email', 'password']
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            if visible.name == 'email':
                visible.field.widget.attrs['placeholder'] = 'Email'
            if visible.name == 'password':
                visible.field.widget.attrs['placeholder'] = 'Password'
                visible.field.widget.attrs['autocomplete'] = 'off'
        return

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        print('---cleaning--')
        if email and password:
            self.user_cache = authenticate(username=email, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(self.message_incorrect_password)
            if not self.user_cache.is_active:
                raise forms.ValidationError(self.message_inactive)
        return self.cleaned_data
    # def confirm_login_allowed(self, user):
    #     pass


class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("email", "password1", "password2")

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            if visible.name == 'email':
                visible.field.widget.attrs['placeholder'] = 'Email'
            if visible.name == 'password1':
                visible.field.widget.attrs['placeholder'] = 'Password'
                visible.field.widget.attrs['autocomplete'] = 'off'
            if visible.name == 'password2':
                visible.field.widget.attrs['placeholder'] = 'Confirm Password'
                visible.field.widget.attrs['autocomplete'] = 'off'
        return

    def save(self, commit=True):
        user = super(UserCreateForm, self).save(commit=False)
        user.email = self.cleaned_data["email"]
        if commit:
            user.save()
        return user


class CustomPasswordResetForm(PasswordResetForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields = ("email", )

    def __init__(self, *args, **kwargs):
        super(CustomPasswordResetForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            if visible.name == 'email':
                visible.field.widget.attrs['placeholder'] = 'Email'
        return


class CustomSetPasswordForm(SetPasswordForm):

    class Meta:
        model = User

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            if visible.name == 'new_password1':
                visible.field.widget.attrs['placeholder'] = 'New Password'
            if visible.name == 'new_password2':
                visible.field.widget.attrs['placeholder'] = 'Confirm Password'
        return


class CustomModelFormMixin(forms.ModelForm):

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            if type(visible.field) == forms.DateField:
                visible.field.widget.attrs['class'] += ' kt_datepicker'
                visible.field.widget.attrs['readonly'] = ''
            if type(visible.field) == forms.ModelChoiceField:
                visible.field.widget.attrs['class'] += ' kt_select2'
                visible.field.widget.attrs['id'] = 'kt_select2_1'
        return


class HistoryItemFormMixin(forms.ModelForm):

    class Meta:
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'
            visible.field.widget.attrs['readonly'] = ''
        return


class TDSApplicableForm(CustomModelFormMixin):

    class Meta:
        model = TDSApplicable
        exclude = ('id',)


class TDSApplicableHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = TDSApplicable
        exclude = ('id',)


class DebitDayDefaultForm(CustomModelFormMixin):

    class Meta:
        model = DebitDayDefault
        exclude = ('id',)


class DebitDayDefaultHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = DebitDayDefault
        exclude = ('id',)


class DebitDayOverrideForm(CustomModelFormMixin):

    class Meta:
        model = DebitDayOverride
        exclude = ('id',)


class DebitDayOverrideHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = DebitDayOverride
        exclude = ('id',)


class ContractMasterForm(CustomModelFormMixin):

    class Meta:
        model = ContractMaster
        exclude = ('id',)


class ContractHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = ContractMaster
        exclude = ('id',)


class MandateMasterForm(CustomModelFormMixin):

    class Meta:
        model = MandateMaster
        exclude = ('id',)


class MandateHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = MandateMaster
        exclude = ('id',)


class MandateDebitOverrideForm(CustomModelFormMixin):
    # mandate = forms.ModelChoiceField()
    # mandate = forms.Select()

    class Meta:
        model = MandateDebitOverride
        exclude = ('id',)


class MandateDebitOverrideHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = MandateDebitOverride
        exclude = ('id',)


class NACHBillingInputForm(CustomModelFormMixin):

    class Meta:
        model = NACHBillingInput
        exclude = ('id', 'billing_month')


class NACHBillingInputHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = NACHBillingInput
        exclude = ('id', 'billing_month')


class NACHProcessedDataForm(CustomModelFormMixin):

    class Meta:
        model = NACHProcessedData
        exclude = ('id', )


class NACHProcessedDataHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = NACHProcessedData
        exclude = ('id',)


class NACHTXNReportForm(CustomModelFormMixin):

    class Meta:
        model = NACHTXNReport
        exclude = ('id', )


class NACHTXNReportHistoryItemForm(HistoryItemFormMixin):

    class Meta:
        model = NACHTXNReport
        exclude = ('id',)
